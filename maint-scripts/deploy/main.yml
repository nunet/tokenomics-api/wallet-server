- name: Install Nix Server
  hosts: all
  become: false
  vars:
    wallet_server_directory: /usr/share/wallet-server
  handlers:
    - name: Reload Wallet server service
      ansible.builtin.systemd:
        daemon_reload: "{{ wallet_server_service.changed }}"
        name: wallet-server
        enabled: true
        state: restarted
  tasks:
    - name: Check that nix command works
      ansible.builtin.command: which nix
      register: is_nix_exist
      ignore_errors: true
      environment:
        PATH: "{{ ansible_env.PATH }}:/nix/var/nix/profiles/default/bin"
      changed_when: false

    - name: Fail if Nix package manager not installed
      ansible.builtin.assert:
        that:
          - is_nix_exist.rc or is_nix_exist.rc == 0
        fail_msg: Nix not installed! Please install nix package manager and try again
        success_msg: Nix installed. Proceeding...

    - name: Copy directory to machine
      ansible.builtin.git:
        repo: https://gitlab.com/nunet/tokenomics-api/wallet-server.git
        dest: /tmp/wallet-server
        force: true
        version: develop

    - name: Start building command
      ansible.builtin.command:
        cmd: nix-shell --run "cabal update && cabal build && cabal install --installdir=. --overwrite-policy=always"
        chdir: /tmp/wallet-server
      environment:
        PATH: "{{ ansible_env.PATH }}:/nix/var/nix/profiles/default/bin"
      changed_when: false

    - name: Elevate permissions and do sudo stuff
      become: true
      block:
        - name: Create config folder for the wallet-server
          ansible.builtin.file:
            state: directory
            path: "{{ wallet_server_directory }}"
            mode: "0744"

        - name: Copy env file to the machine
          ansible.builtin.template:
            src: templates/env.j2
            dest: "{{ wallet_server_directory }}/.env"
            mode: "0640"

        - name: Copy systemd unit file to the machine
          ansible.builtin.template:
            src: files/wallet-server.service
            dest: /etc/systemd/system/wallet-server.service
            mode: "0644"
          register: wallet_server_service

        - name: Copy wallet server binary to the bin folder
          ansible.builtin.copy:
            src: /tmp/wallet-server/wallet-server
            remote_src: true
            dest: "{{ wallet_server_directory }}/wallet-server"
            mode: "0755"
          register: wallet_server_binary
          notify: Reload Wallet server service
