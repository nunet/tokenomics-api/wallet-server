## Introduction
This documentation provides detailed descriptions of the modules `Main.hs` and `Wallet.hs`. These modules are primarily focused on web services, networking, and message signing functionalities.
## Table of Contents
- [Main.hs Overview](#mainhs-overview)
- [Wallet.hs Overview](#walleths-overview)
- [Dependencies](#dependencies)
- [Data Types](#data-types)
- [Functions](#functions)
- [Key Functionalities](#key-functionalities)
- [Usage Context](#usage-context)

## Main.hs Overview

### Dependencies
- **Web.Scotty**: For building web applications.
- **Network.HTTP.Simple**: Simplified HTTP client.
- **Data.Aeson**: For JSON parsing.
- **PlutusTx**: Plutus smart contract utilities.

### Data Types
1. **ServiceRequest**
2. **OracleResponse**
3. **RewardRequest**
4. **RewardResponse**
5. **VaultClient**

### Functions
- `parseToken`
- `getSecretToken`
- `randomString`
- `stringToDatumHash`
- `extractSignature`, `extractMessageHash`, `extractDatum`
- `processServiceRequest`
- `processRewardRequest`
- `tryGetToken`
- `main`

### Key Functionalities
- Service Request Processing
- Reward Request Handling
- Token Retrieval and Management

---

## Wallet.hs Overview
`Wallet.hs` provides utilities for wallet operations related to blockchain and smart contracts, including message signing and private key management.

### Functions
- `signRedeemActionToken`
- `stringToStrictByteString`
- `generateWallet`
- `extractPrivateKey`

### Key Functionalities
- Message Signing
- Wallet Generation
- Private Key Extraction