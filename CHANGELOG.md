<!-- New changes go on top. But below these comments. -->

<!-- We can track changes from Git logs, so why we needs this file?

Guiding Principles
- Changelogs are for humans, not machines.
- There should be an entry for every single version.
- The same types of changes should be grouped.
- Versions and sections should be linkable.
- The latest version comes first.
- The release date of each version is displayed.
- Mention whether you follow Semantic Versioning.

Types of changes
- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

-->
# https://gitlab.com/nunet/tokenomics-api/tokenomics-api-cardano/-/merge_requests/17

### Fixed
- Changed the wallet server api
- Removed the deadline as a parameter 
- Added endpoint for calculation of hashes 
- Added endpoint for signatures of datum and action hash 
