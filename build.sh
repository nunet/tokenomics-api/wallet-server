#!/bin/bash

# Build the Haskell project using nix-shell
nix-shell --run "cabal build && cabal install --installdir=. --overwrite-policy=always"

# Any other commands here are executed outside of nix-shell
echo "Haskell project built and binary built"

./wallet-server

