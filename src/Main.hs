{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Web.Scotty
import Network.HTTP.Simple 
import qualified Data.Text.Lazy as TL

import qualified Data.HashMap.Strict as HM

import Network.HTTP.Types.Status 
import Data.Aeson 
import Data.Aeson.Types as Types -- Add this line
import Data.Aeson (Value)

import Data.Text.Lazy.Encoding (decodeUtf8, encodeUtf8)
import Control.Monad.IO.Class (liftIO)
import Data.Text.Lazy 
import Wallet 
import PlutusTx.Builtins 
import qualified Data.ByteString.Char8 as BSC
import PlutusTx.Builtins.Class (stringToBuiltinByteString)
import Data.ByteString (ByteString, pack)
import System.Random (randomRIO)
import PlutusTx (toBuiltinData)
import PlutusTx.Prelude (BuiltinByteString)
import Ledger.Crypto 
import Plutus.Contract.Oracle 
import Ledger 
import Plutus.V1.Ledger.Time as TimeV1
import Plutus.V1.Ledger.Api 
import Control.Monad (replicateM)
import Data.String (IsString, fromString)
import PlutusTx.IsData.Class
import Control.Monad.Logger 
import Control.Monad.IO.Class (liftIO)
import Data.Text (Text)
import Data.Text.IO as TIO
import Data.ByteString.Builder (toLazyByteString)
import Data.ByteString.Lazy (toStrict)
import Data.Text.Lazy.Encoding (decodeUtf8)
import System.Log.FastLogger (LogStr, fromLogStr)
import qualified Data.Text as T
import Data.Aeson (encode)
import Data.Text.Encoding (decodeUtf8')
import Data.Either.Combinators (fromRight')
import qualified Data.ByteString.Lazy.Char8 as BLC
import Data.ByteString.Lazy (toStrict)
import System.Environment (lookupEnv)
import qualified Configuration.Dotenv as Dotenv
import GHC.Generics
import qualified Data.ByteString.Char8 as BS

convertStringToByteString :: String -> BS.ByteString
convertStringToByteString = BS.pack
-- Function to extract the seed phrase

data ServiceRequest = ServiceRequest
  { user            :: String
  , computeProvider :: String
  , ntxAmount       :: Integer
  } deriving (Show)

instance FromJSON ServiceRequest where
  parseJSON = withObject "ServiceRequest" $ \v ->
    ServiceRequest <$> v .: "user"
                   <*> v .: "computeProvider"
                   <*> v .: "ntxAmount"

data OracleResponse = OracleResponse
  { 
    metaDataHash       :: DatumHash
  , withdrawHash       :: DatumHash
  , refundHash         :: DatumHash
  , distribute50Hash     :: DatumHash
  , distribute75Hash     :: DatumHash
  } deriving (Show)

instance ToJSON OracleResponse where
  toJSON (OracleResponse m w r di50 di75) = object
    [ 
     "metaDataHash" .= show m
    , "withdrawHash" .= show w
    , "refundHash" .= show r
    , "distribute50Hash" .= show di50
    ,  "distribute75Hash" .= show di75
    ]


data RewardRequest = RewardRequest
  { dataHash :: DatumHash
  , actionHash     :: DatumHash
  } deriving (Show)

instance FromJSON RewardRequest where
  parseJSON = withObject "RewardRequest" $ \v ->
    RewardRequest <$> v .: "metaDataHash"
                  <*> v .: "actionHash"


instance ToJSON RewardRequest where
  toJSON (RewardRequest dataHash actionHash) = object
    [ "metaDataHash" .= dataHash
    , "actionHash"     .= actionHash 
    ]



data RewardResponse = RewardResponse
  { sigData        :: Signature
  , sigDataHash    :: DatumHash
  , sigDataDatum   :: String
  , sigAction      :: Signature
  , sigActionHash  :: DatumHash
  , sigActionDatum :: String
  } deriving (Show)


instance ToJSON RewardResponse where
  toJSON (RewardResponse sigData sigDataHash sigDataDatum sigAction sigActionHash sigActionDatum) = object
    [ "sigData" .= show sigData
    , "sigDataHash" .= show sigDataHash
    , "sigDataDatum" .= show sigDataDatum
    , "sigAction" .= show sigAction
    , "sigActionHash" .= show sigActionHash
    , "sigActionDatum" .= show sigActionDatum
    ]


-- | Represents a simple Vault client
data VaultClient = VaultClient
  { vaultUrl :: String
  , vaultToken :: Maybe String
  } deriving (Show, Eq)


 --Define a data structure to match the JSON response
data SeedPhraseResponse = SeedPhraseResponse
  { dataField :: DataField
  } deriving (Show)

instance FromJSON SeedPhraseResponse where
  parseJSON = withObject "SeedPhraseResponse" $ \v ->
    SeedPhraseResponse <$> v .: "data"

data DataField = DataField
  { dataDataField :: DataDataField
  } deriving (Show)

instance FromJSON DataField where
  parseJSON = withObject "DataField" $ \v ->
    DataField <$> v .: "data"

data DataDataField = DataDataField
  { seedPhrase :: String
  } deriving (Show)

instance FromJSON DataDataField where
  parseJSON = withObject "DataDataField" $ \v ->
    DataDataField <$> v .: "seed_phrase"



parseToken :: Types.Value -> Maybe String
parseToken = parseMaybe parseJSONToken
  where
    parseJSONToken :: Types.Value -> Parser String
    parseJSONToken = withObject "response" $ \obj -> do
      auth <- obj .: "auth"
      auth .: "client_token"
-- | Retrieves a secret token
-- | Tries to retrieve a secret token, with one retry on failure
getSecretToken :: String -> String -> IO (Maybe String)
getSecretToken  roleId secretId =  do
      let url = "https://vault.nunet.io/v1/auth/approle/login"
          params = [ ("role_id", toJSON $ T.pack roleId)
                   , ("secret_id", toJSON $ T.pack secretId) ]
      request <- parseRequest $ "POST " ++ url
      let request' = setRequestBodyJSON (object params) request
      response <- httpJSON request'
      let responseBody = getResponseBody response ::Types.Value
      return $ parseToken responseBody


-- Function to fetch data from the Vault
fetchVaultData :: String ->IO (Maybe Data.Aeson.Value)
fetchVaultData token = do
  let bytetoken = convertStringToByteString token
  let request = setRequestMethod "GET"
              $ setRequestHeader "X-Vault-Token" [bytetoken]
              $ setRequestSecure True
              $ setRequestPort 443
              $ setRequestPath "/v1/oracle/data/haskell"
              $ setRequestHost "vault.nunet.io"
              $ defaultRequest
  response <- httpJSON request
  return $ getResponseBody response

-- Adjust the extractSeedPhrase to directly work with Data.Aeson.Value
extractSeedPhrase :: Maybe Data.Aeson.Value -> Maybe T.Text
extractSeedPhrase (Just (Object obj)) = do
    -- Drill down through the nested objects to the "seed_phrase" field
    data1 <- HM.lookup "data" obj >>= \(Object obj') -> Just obj'
    data2 <- HM.lookup "data" data1 >>= \(Object obj'') -> Just obj''
    seedPhrase <- HM.lookup "seed_phrase" data2 >>= \(String s) -> Just s
    return seedPhrase
extractSeedPhrase _ = Nothing

randomString :: Int -> IO String
randomString len = replicateM len $ randomRIO ('a', 'z')

stringToDatumHash :: String -> DatumHash
stringToDatumHash ss= fromString ss :: DatumHash

extractSignature :: SignedMessage a -> Signature
extractSignature (SignedMessage sig _ _) = sig

extractMessageHash :: SignedMessage a-> DatumHash
extractMessageHash (SignedMessage _ hash _) = hash

extractDatum:: SignedMessage a -> String 
extractDatum  (SignedMessage _ _ dat) = show $ getDatum $ dat

processServiceRequest :: ServiceRequest -> IO OracleResponse
processServiceRequest (ServiceRequest user computeProvider ntxAmount) = do
  let ntxAmountString = stringToBuiltinByteString $ show $ ntxAmount
      userString = stringToBuiltinByteString user
      computeProviderString =  stringToBuiltinByteString computeProvider
      metaDataString = PlutusTx.Builtins.encodeUtf8  (appendString (appendString  (PlutusTx.Builtins.decodeUtf8  userString) (PlutusTx.Builtins.decodeUtf8 computeProviderString)) (PlutusTx.Builtins.decodeUtf8 ntxAmountString))
      metaDataHash = DatumHash $  metaDataString
  randomStr1 <- randomString 10
  randomStr2 <- randomString 10
  randomStr3 <- randomString 10
  randomStr4 <- randomString 10
  randomStr5 <- randomString 10
  let randomStr1Builtin = stringToBuiltinByteString randomStr1
      randomStr2Builtin = stringToBuiltinByteString randomStr2
      randomStr3Builtin = stringToBuiltinByteString randomStr3
      randomStr4Builtin = stringToBuiltinByteString randomStr4
      randomStr5Builtin = stringToBuiltinByteString randomStr5

  let withdrawHash = DatumHash $  randomStr1Builtin
      refundHash = DatumHash $   randomStr2Builtin
      distribute50Hash = DatumHash $ randomStr4Builtin
      distribute75Hash = DatumHash $ randomStr5Builtin
  return $ OracleResponse  metaDataHash withdrawHash refundHash distribute50Hash distribute75Hash

processRewardRequest ::T.Text -> RewardRequest -> IO RewardResponse
processRewardRequest seedPhrase (RewardRequest dataHash actionHash)  = do
  let stringSeed =T.unpack seedPhrase
  let wallet = generateWallet stringSeed
      privateKey = extractPrivateKey wallet
      -- Choose the action based on the input parameter
    
      -- Sign the messages
      signedMessageDatum = signMessage' dataHash privateKey
      signedMessageAction = signMessage' actionHash privateKey

      sigData = extractSignature signedMessageDatum
      sigAction = extractSignature signedMessageAction

      sigDataDatum = extractDatum signedMessageDatum
      sigActionDatum = extractDatum signedMessageAction

      metaDataMessageHash = extractMessageHash signedMessageDatum
      actionMessageHash = extractMessageHash signedMessageAction

  return $ RewardResponse sigData metaDataMessageHash sigDataDatum sigAction actionMessageHash sigActionDatum

tryGetToken :: Int -> String -> String -> IO (Maybe String)
tryGetToken 0 _ _ = return Nothing

tryGetToken n roleId secretId = do
  maybeToken <- getSecretToken roleId secretId
  case maybeToken of
    Just token -> return $ Just token
    Nothing -> tryGetToken (n-1) roleId secretId

main :: IO ()
main =  do
  _ <- Dotenv.loadFile Dotenv.defaultConfig { Dotenv.configPath = [".env"] }
  maybeSecretId <- lookupEnv "secret_id"
  maybeRoleId <- lookupEnv "role_id"
  maybeToken <- case (maybeRoleId, maybeSecretId) of
    (Just roleId, Just secretId) -> tryGetToken 2 roleId secretId
    _ -> return Nothing

  maybeJsonData <- case maybeToken of
    Just token -> fetchVaultData token
    _ -> return Nothing 

  maybeSeedPhrase <- return $ extractSeedPhrase maybeJsonData

  case maybeSeedPhrase of
    Just token ->
      scotty 3877 $ do
        post "/request_service" $ do
          requestBody <- body
          
          case decode requestBody :: Maybe ServiceRequest of
            Just serviceRequest -> do
              oracleResponse <- liftIO $ processServiceRequest serviceRequest
              Web.Scotty.json oracleResponse
            Nothing -> do
              status badRequest400
              liftIO $ runStdoutLoggingT $ logInfoN $ fromRight' . decodeUtf8' . BLC.toStrict $  requestBody
              text "Invalid request payload"
              
        post "/request_reward" $ do
          requestBody <- body
          case eitherDecode requestBody :: Either String RewardRequest of
            Right payload -> do
              rewardResponse <- liftIO $ processRewardRequest token payload
              Web.Scotty.json rewardResponse
            Left err -> do
              status badRequest400
              text $ TL.pack err

    Nothing -> do  
      Prelude.putStrLn "Failed to retrieve token from Vault at startup"
